import json, pinyin #pip install pinyin

with open('pinyin2api.json') as inFile:
    pinyin2api = json.load(inFile)

with open('api2class.json') as inFile:
    api2class = json.load(inFile)


##### LISTES EXCEPTIONS #####
xjq = ['x','j','q'] # suivis de -u → /-y/
uun = ['u','un'] # précédés de xjq font /y/
ilist = ['z','c','s','zh','ch','sh','r'] # -i is a buzzed continuation of the consonant following z-, c-, s-, zh-, ch-, sh- or r-.
avoidlist = ['kw', 'wa'] # éviter de les considérer comme un seul phonème

inp = '銀行'

pin = pinyin.get(inp, format="numerical", delimiter=" ")
print(pin)
# ni3hao3

charlist = [] # [ ('ni',3), ('hao',3) ]
pinparse = pin.split(' ')

result = []
h = 0
for carton in pinparse:
    hanzi = inp[h]
    h += 1
    car = carton[:-1]
    ton = carton[-1]

    if car in pinyin2api.keys():
        api = pinyin2api[car]
    else:
        if car[0] in pinyin2api.keys():
            api = pinyin2api[car[0]]
            if car[1:] in pinyin2api.keys():
                if car[0] in xjq and car[1:] in uun:
                    api += pinyin2api['_' + car[1:]]
                elif car[0] in ilist and car[1:] == 'i':
                    api += pinyin2api['_' + car[1:]]
                else:
                    api += pinyin2api[car[1:]]
            else:
                api = ""
                print(car[0], "trouvé ;", car[1:], "non trouvé!")
        else:
            print(car[0], "non trouvé!")

    ## Conversion api2class
    phonlist = []
    
    phon = ''
    restapi = api
    while len(restapi) > 0:
        i=0
        trouve = True
        while trouve and i<=len(restapi):
            i+=1
            if restapi[:i] in api2class.keys() and restapi[:i] not in avoidlist:
                continue
                #print(i, restapi[:i])
            else:
                i = i-1
                trouve = False
        #print("trouvé",restapi[:i])
        phonlist.append(api2class[restapi[:i]])

        restapi = restapi[i:]
    
    result.append((hanzi, api, phonlist, ton))


print(result)
